 /*
 *Name:scripts.js
 *description:Add animations to page
 *author : abhiyan timilsina 
 
 */

//Initializing global variable
let header = document.querySelector(".header");
let menuItems  = document.getElementsByClassName(".menu-item");
let toggleButton = document.getElementById("nav-toggle");
let sideMenu = document.getElementById("side-menu");
let desktopNav = document.getElementById("desktop_nav");
let prev_scroll_y = 0;

//Event fired when the user scrolls 
window.addEventListener('scroll',()=>{
    if(window.scrollY<=100) {
        header.removeAttribute('class');
        header.setAttribute('class','header');
      }
    else if(window.scrollY<prev_scroll_y) {
      header.removeAttribute('class');
      header.setAttribute('class','header_scrolled');
    }
   
    else {
        prev_scroll_y = window.scrollY;
    }
});

//Listening to click event on toggle button
 toggleButton.addEventListener('click',()=>{
     sideMenu.appendChild(desktopNav);
     sideMenu.style.width = '60%';
 });

 

 //Taking skills and calculating percentage 1% is 1.56px
  var skills = [{
      name : 'HTML',
      percentage: 80,
      level : 'Intermidiate',
      imageURL : 'html.png'
   },{
    name : 'CSS',
    percentage: 60,
    level : 'Intermidiate',
    imageURL : 'css.png'
  },{
    name : 'JS',
    percentage: 80,
    level : 'Intermidiate',
    imageURL : 'js.png'
  },{
    name : 'Databases',
    percentage: 50,
    level : 'Intermidiate',
    imageURL : 'database.jpg'
 },
 {
    name : 'Angular',
    percentage: 40,
    level : 'Beginner',
    imageURL : 'angular.png'
},{
    name : 'Python',
    percentage: 10,
    level : 'Noobie',
    imageURL : 'python.png'

},{
    name : 'Agile',
    percentage: 10,
    level : 'Noobie',
    imageURL : 'agile.jpg'
},{
    name : 'Communication',
    percentage: 50,
    level : 'Avrage',
    imageURL : 'communication.jpg'
},{
    name : 'NodeJS',
    percentage: 70,
    level : 'Intermidiate',
    imageURL : 'node.png'
},];
var basePath = './images/'

let skillSection = document.querySelector('.skills');
skills.forEach((skill)=>{

    //Creating DOM Elements
  let container = document.createElement('div');
  let image = document.createElement('img');

  image.setAttribute('src',basePath+skill.imageURL);
  let title = document.createElement('p');
  let progressContainer = document.createElement('div');
  let progress = document.createElement('div');
 
  //Defining CSS classes
  container.setAttribute('class','skill-items');
  progressContainer.setAttribute('class','progress-bar');
  progress.setAttribute('class','progress');
  
  //Initializing elements
  progress.innerText=skill['level'];
  if(skill.percentage < 50) {
      progress.style.backgroundColor = 'red';
      progress.style.color = 'white';
  }
  let progress_per_to_width = Number(skill['percentage']) * 1.56;
  progress.style.maxWidth = progress_per_to_width+'px'; 
  
  //Adding elements in hierarchy
 progressContainer.appendChild(progress);
  title.innerText=skill['name'];
  container.appendChild(title);
  container.appendChild(image);
  container.appendChild(progressContainer);
  skillSection.appendChild(container);
});